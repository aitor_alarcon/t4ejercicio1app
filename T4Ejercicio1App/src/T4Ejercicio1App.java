/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T4Ejercicio1App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creamos las dos variables numéricas
		double num1 = 65;
		double num2 = 15;
		
		// Calculamos la suma
		double suma = num1 + num2;
		// Calculamos la resta
		double resta = num1 - num2;
		// Calculamos la multiplicacion
		double multiplicacion = num1 * num2;
		// Calculamos la division
		double division = num1 / num2;
		// Calculamos el modulo
		double modulo = num1 % num2;
		
		// Mostramos todos los resultados
		System.out.println("Suma = " + suma);
		
		System.out.println("Resta = " + resta);
		
		System.out.println("Multiplicación = " + multiplicacion);
		
		System.out.println("Division = " + division);
		
		System.out.println("Módulo = " + modulo);
	}

}
